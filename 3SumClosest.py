class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        best = 1e9
        nums = sorted(nums)
        for i, x in enumerate(nums):
            k = len(nums) - 1
            for j in xrange(i + 1, len(nums)):
                y = nums[j]
                while k > j + 1 and y + nums[k] > target-x:
                    k -= 1
                if k <= j: continue
                if y + nums[k] > target-x:
                    if abs(y+nums[k]+x-target) < abs(best-target):
                        best = y+nums[k]+x
                else:
                    if abs(y+nums[k]+x-target) < abs(best-target):
                        best = y+nums[k]+x
                    if k < len(nums)-1:
                        k += 1
                    if abs(y+nums[k]+x-target) < abs(best-target):
                        best = y+nums[k]+x
        return best


if __name__ == "__main__":
    print Solution().threeSumClosest(nums = [-1, 2, 1, -4], target = 1)
