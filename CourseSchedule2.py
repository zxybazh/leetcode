class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        rd = [0 for i in xrange(numCourses)]
        e = [[] for i in xrange(numCourses)]
        cur = set()
        ans = []
        for t in prerequisites:
            e[t[1]].append(t[0])
            rd[t[0]] += 1
        for i, x in enumerate(rd):
            if x == 0:
                cur.add(i)
        while len(ans) != numCourses:
            if len(cur) > 0:
                i = cur.pop()
                ans.append(i)
                for w in e[i]:
                    rd[w] -= 1
                    if rd[w] == 0:
                        cur.add(w)
            else:
                return []
        return ans

if __name__ == "__main__":
    print Solution().findOrder(2, [[1,0]])
    print Solution().findOrder(2, [[1, 0], [0, 1]])
    print Solution().findOrder(4, [[1,0],[2,0],[3,1],[3,2]])
