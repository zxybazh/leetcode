class Solution(object):
    def search(self, matrix, target, lx, rx, ly, ry):
        if matrix == [] or lx > rx or ly > ry: return False
        #print [w[ly:ry+1] for w in matrix[lx:rx+1]], matrix[(lx+rx)/2][(ly+ry)/2], lx, rx, ly, ry
        if matrix[(lx+rx)/2][(ly+ry)/2] == target: return True
        elif lx == rx and ly == ry: return False
        elif matrix[(lx+rx)/2][(ly+ry)/2] > target:
            return self.search(matrix, target, lx, (lx+rx)/2-1, ly, ry) or \
                   self.search(matrix, target, (lx+rx)/2, rx, ly, (ly+ry)/2-1)
        else:
            return self.search(matrix, target, lx, rx, (ly+ry)/2+1, ry) or\
                   self.search(matrix, target, (lx+rx)/2+1, rx, ly, (ly+ry)/2)

    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        if matrix == []: return False
        return self.search(matrix, target, 0, len(matrix)-1, 0, len(matrix[0])-1)


if __name__ == "__main__":
    print Solution().searchMatrix(
        [
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 10],
            [11, 12, 13, 14, 15],
            [16, 17, 18, 19, 20],
            [21, 22, 23, 24, 25]
        ], 5
    )
    print Solution().searchMatrix(
        [
            [1, 4, 7, 11, 15],
            [2, 5, 8, 12, 19],
            [3, 6, 9, 16, 22],
            [10, 13, 14, 17, 24],
            [18, 21, 23, 26, 30]
        ], 5
    )
    print Solution().searchMatrix(
        [
            [1, 4, 7, 11, 15],
            [2, 5, 8, 12, 19],
            [3, 6, 9, 16, 22],
            [10, 13, 14, 17, 24],
            [18, 21, 23, 26, 30]
        ], 20
    )
