class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if nums.count(0) >= 3:
            ans = [[0, 0, 0]]
        else:
            ans = []
        last = None
        nums = sorted(nums)
        for i, x in enumerate(nums):
            if x != 0 and x != last and nums.count(x) >= 2 and nums.count(-x-x) > 0:
                ans += [[x, x, -x-x]]
            last = x
        nums = sorted(list(set(nums)))
        for i, x in enumerate(nums):
            k = len(nums)-1
            for j in xrange(i+1, len(nums)):
                y = nums[j]
                while k > j+1 and y + nums[k] > -x:
                    k -= 1
                if k > j and y + nums[k] == -x:
                    ans += [[x, y, nums[k]]]
        return ans


if __name__ == "__main__":
    print Solution().threeSum(nums = [-1, 0, 1, 2, -1, -4])
