class Solution(object):
    def countBinarySubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s[0] == '0': x = '1'
        else: x = '0'
        t = []
        for w in s:
            if w == x:
                t[-1][1] += 1
            else:
                x = w
                t.append([x, 1])
        ans = 0
        for i in xrange(len(t)-1):
            ans += min(t[i][1], t[i+1][1])
        return ans


if __name__ == "__main__":
    print Solution().countBinarySubstrings("00110011")
    print Solution().countBinarySubstrings("10101")

