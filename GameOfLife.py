class Solution(object):
    def judge(self, current, liveNeighbours):
        if current == 1:
            if liveNeighbours == 2 or liveNeighbours == 3:
                return 1
            else:
                return 0
        else:
            if liveNeighbours == 3:
                return 1
            return 0

    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        dx = [0, 1, 0, -1, 1, -1, 1, -1]
        dy = [1, 0, -1, 0, 1, -1, -1, 1]
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                ne = 0
                for fx in xrange(8):
                    x = i + dx[fx]
                    y = j + dy[fx]
                    if 0 <= x < len(board) and 0 <= y < len(board[0]) and board[x][y] % 10000 == 1:
                        ne += 1
                board[i][j] += self.judge(board[i][j]%10000, ne)*10000
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                board[i][j] /= 10000


if __name__ == "__main__":
    print Solution().gameOfLife([
      [0,1,0],
      [0,0,1],
      [1,1,1],
      [0,0,0]
    ])
