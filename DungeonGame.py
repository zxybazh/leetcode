class Solution(object):
    def calculateMinimumHP(self, dungeon):
        """
        :type dungeon: List[List[int]]
        :rtype: int
        """
        f = [[1e9 for j in dungeon[0]] for i in dungeon]
        for i in xrange(len(dungeon)-1, -1, -1):
            for j in xrange(len(dungeon[0])-1, -1, -1):
                if i == len(dungeon)-1 and j == len(dungeon[0])-1:
                    f[i][j] = max(1, 1-dungeon[i][j])
                else:
                    if j+1 < len(dungeon[0]):
                        f[i][j] = min(f[i][j], max(1, 1-dungeon[i][j], -dungeon[i][j]+f[i][j+1]))
                    if i+1 < len(dungeon):
                        f[i][j] = min(f[i][j], max(1, 1-dungeon[i][j], -dungeon[i][j]+f[i+1][j]))
        return f[0][0]


if __name__ == "__main__":
    print Solution().calculateMinimumHP([[-2, -3, 3], [-5, -10, 1], [10, 30, -5]])
