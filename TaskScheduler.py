class Solution(object):
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        tasks = sorted([tasks.count(x) for x in set(tasks)], reverse = True)
        ans = (tasks[0] - 1) * (n + 1) + tasks.count(tasks[0])
        return max(ans, sum(tasks))

if __name__ == "__main__":
    print Solution().leastInterval(tasks = ["A","A","A","B","B","B"], n = 2)
