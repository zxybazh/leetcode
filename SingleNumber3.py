class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        x = 0
        for w in nums: x ^= w
        t = 1
        while x & t == 0:
            t <<= 1
        x0 = 0
        x1 = 0
        for w in nums:
            if w & t == 0:
                x0 ^= w
            else:
                x1 ^= w
        return [x0, x1]

if __name__ == "__main__":
    print Solution().singleNumber([1,2,1,3,2,5])
