class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if matrix == []: return []
        dx = [0, 1, 0, -1]
        dy = [1, 0, -1, 0]
        ans = []
        x = 0
        y = 0
        fx = 0
        used = [[False for i in matrix[0]] for j in matrix]
        while len(ans) < len(matrix) * len(matrix[0]):
            ans.append(matrix[x][y])
            used[x][y] = True
            tx = x + dx[fx]
            ty = y + dy[fx]
            #print x, y
            if len(ans) < len(matrix) * len(matrix[0]):
                while tx < 0 or tx >= len(matrix) or ty < 0 or ty >= len(matrix[0]) or used[tx][ty]:
                    fx = (fx + 1) % 4
                    tx = x + dx[fx]
                    ty = y + dy[fx]
                x = tx
                y = ty
        return ans



if __name__ == "__main__":
    print Solution().spiralOrder(
        [[1]]
    )
    print Solution().spiralOrder(
        [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
    )
    print Solution().spiralOrder(
        [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12]
        ]
    )
