class Solution(object):
    def getVal(self, x):
        if x == 'I':
            return 1
        elif x == 'V':
            return 5
        elif x == 'X':
            return 10
        elif x == 'L':
            return 50
        elif x == 'C':
            return 100
        elif x == 'D':
            return 500
        else:
            return 1000
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        ts = []
        for w in s:
            if ts == [] or ts[-1][0] != w:
                ts.append([w, 1])
            else:
                ts[-1][1] += 1
        ans = 0
        for i, x in enumerate(ts):
            ans += x[1] * self.getVal(x[0]) * (-1 if i < len(ts)-1 and self.getVal(x[0])<self.getVal(ts[i+1][0]) else 1)
        return ans

if __name__ == "__main__":
    print Solution().romanToInt("III")
    print Solution().romanToInt("IV")
    print Solution().romanToInt("LVIII")
    print Solution().romanToInt("MCMXCIV")
