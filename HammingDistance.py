class Solution(object):
    def hammingDistance(self, x, y):
        """
        :type x: int
        :type y: int
        :rtype: int
        """
        z = x^y
        cnt = 0
        while z > 0:
            cnt += 1
            z -= z & -z
        return cnt


if __name__ == "__main__":
    print Solution().hammingDistance(1, 4)
