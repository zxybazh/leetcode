# Definition for a binary tree node.
class TreeNode(object):
     def __init__(self, x):
         self.val = x
         self.left = None
         self.right = None

class Solution(object):
    def dfs(self, x, depth):
        if x == None: return
        if depth+1 > len(self.ans):
            self.ans.append([x.val])
        else:
            self.ans[depth].append(x.val)
        self.dfs(x.left, depth+1)
        self.dfs(x.right, depth+1)

    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self.ans = []
        self.dfs(root, 0)
        return self.ans

