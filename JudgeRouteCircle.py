class Solution(object):
    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        return moves.count("L") == moves.count("R") and moves.count("U") == moves.count("D")


if __name__ == "__main__":
    print Solution().judgeCircle("UD")
    print Solution().judgeCircle("LL")
