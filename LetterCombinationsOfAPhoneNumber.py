class Solution(object):
    def dfs(self, depth, digits, cur):
        if depth == len(digits):
            if cur != "":
                self.ans.append(cur)
            return
        x = int(digits[depth])
        if self.cls[x] == "":
            self.dfs(depth+1, digits, cur)
        else:
            for w in self.cls[x]:
                self.dfs(depth+1, digits, cur+w)

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        self.ans = []
        self.cls = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        self.dfs(0, digits, "")
        return self.ans


if __name__ == "__main__":
    print Solution().letterCombinations("23")
    print Solution().letterCombinations("")
