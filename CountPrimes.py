class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        p = [True for i in xrange(n)]
        if n > 1: p[0] = p[1] = False
        primes = []
        for i in xrange(2, n):
            if p[i]:
                primes.append(i)
                for j in xrange(i, n, i):
                    p[j] = False
        return len(primes)


if __name__ == "__main__":
    print Solution().countPrimes(0)
    print Solution().countPrimes(2)
    print Solution().countPrimes(10)
    print Solution().countPrimes(100)
