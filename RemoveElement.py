class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        for i in xrange(len(nums)):
            for j in xrange(i+1, len(nums)):
                if nums[i] == val and nums[j] != val:
                    temp = nums[i]
                    nums[i] = nums[j]
                    nums[j] = temp
        cnt = len(nums)
        for w in nums:
            if w == val:
                cnt -= 1
        return cnt


if __name__ == "__main__":
    print Solution().removeElement(nums = [3,2,2,3], val = 3)
    print Solution().removeElement(nums = [0,1,2,2,3,0,4,2], val = 2)
