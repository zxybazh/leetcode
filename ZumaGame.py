class Solution(object):
    def clear(self, x):
        flag = None
        for i in xrange(len(x) - 2):
            if x[i] == x[i+1] and x[i+1] == x[i+2]:
                flag = i
                break
        while flag is not None:
            j = flag + 1
            while j < len(x) and x[j] == x[flag]: j += 1
            x = x[:flag] + x[j:]
            flag = None
            for i in xrange(len(x)-2):
                if x[i] == x[i+1] and x[i+1] == x[i+2]:
                    flag = i
                    break
        return x

    def dfs(self, board, depth, hand):
        #print board, hand, depth
        if board == "":
            return depth
        if hand == "":
            return 1e9
        used = set()
        res = 1e9
        for yyy, xxx in enumerate(hand):
            if xxx in used: continue
            used.add(xxx)
            if xxx not in board:
                res = min(res, self.dfs(xxx+board, depth+1, hand[:yyy]+hand[yyy+1:]))
            else:
                i = 0
                while i < len(board):
                    if board[i] == xxx:
                        if i+1 < len(board) and board[i+1] == xxx:
                            tboard = self.clear(board[:i]+board[i+2:])
                            res = min(res, self.dfs(tboard, depth+1, hand[:yyy]+hand[yyy+1:]))
                            i += 1
                        else:
                            res = min(res, self.dfs(board[:i]+xxx+board[i:], depth+1, hand[:yyy]+hand[yyy+1:]))
                    i += 1
        return res

    def findMinStep(self, board, hand):
        """
        :type board: str
        :type hand: str
        :rtype: int
        """
        #print '-'*10
        if board == "": return 0
        res = self.dfs(board, 0, hand)
        if res == 1e9:
            res = -1
        return res


if __name__ == "__main__":
    print Solution().findMinStep("WRRBBW", "RB")
    print Solution().findMinStep("WWRRBBWW", "WRBRW")
    print Solution().findMinStep("G", "GGGGG")
    print Solution().findMinStep("RBYYBBRRB", "YRBGB")
