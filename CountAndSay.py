class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        s = '1'
        for i in xrange(n-1):
            t = ''
            while len(s) > 0:
                cnt = 0
                for j in xrange(1, len(s)):
                    if s[j] != s[0]:
                        cnt = j-1
                        break
                    else:
                        cnt += 1
                t += str(cnt+1)+s[0]
                s = s[cnt+1:]
            s = t
        return s


if __name__ == "__main__":
    print Solution().countAndSay(1)
    print Solution().countAndSay(5)
