class Solution(object):
    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        s = list(s)
        i = 0
        j  = len(s)-1
        while (i < j):
            while i < j and s[i] not in 'aeiouAEIOU': i+=1
            while i < j and s[j] not in 'aeiouAEIOU': j-=1
            if i >= j: break
            s[i], s[j] = s[j], s[i]
            i += 1
            j -= 1
            #print s
        return "".join(s)


if __name__ == "__main__":
    print Solution().reverseVowels("hello")
    print Solution().reverseVowels("leetcode")
