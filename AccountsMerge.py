class Solution(object):
    def find(self, x):
        if self.fa[x] == x:
            return x
        self.fa[x] = self.find(self.fa[x])
        return self.fa[x]

    def accountsMerge(self, accounts):
        """
        :type accounts: List[List[str]]
        :rtype: List[List[str]]
        """
        d = {}
        for i, w in enumerate(accounts):
            for j in w[1:]:
                if j in d:
                    d[j].append(i)
                else:
                    d[j] = [i]
        self.fa = [i for i in xrange(len(accounts))]
        for email in d:
            for i in xrange(1, len(d[email])):
                self.fa[self.find(d[email][i])] = self.find(d[email][0])
        ans = []
        for i, w in enumerate(accounts):
            if self.find(i) != i:
                accounts[self.find(i)] += w[1:]
        for i, w in enumerate(accounts):
            if self.find(i) == i:
                ans.append(accounts[i][:1]+sorted(list(set(accounts[i][1:]))))
        return ans


if __name__ == "__main__":
    print Solution().accountsMerge(accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]])
    print Solution().accountsMerge([["David","David0@m.co","David1@m.co"],["David","David3@m.co","David4@m.co"],["David","David4@m.co","David5@m.co"],["David","David2@m.co","David3@m.co"],["David","David1@m.co","David2@m.co"]])
