class Solution(object):
    def exclusiveTime(self, n, logs):
        """
        :type n: int
        :type logs: List[str]
        :rtype: List[int]
        """
        events = []
        for w in logs:
            w = w.split(":")
            events.append((int(w[2]), 0 if w[1] == "start" else 1, int(w[0])))
        ans = [0 for i in set(w[2] for w in events)]
        stack = []
        events = sorted(events)
        for e in events:
            time, status, no = e
            if status == 0:
                if len(stack) > 0:
                    ans[stack[-1][0]] += time - stack[-1][1]
                stack.append((no, time))
            else:
                ans[stack[-1][0]] += time - stack[-1][1]+1
                stack = stack[:-1]
                if len(stack) > 0: stack[-1] = (stack[-1][0], time+1)
        return ans




if __name__ == "__main__":
    print Solution().exclusiveTime(n = 2, logs = ["0:start:0", "1:start:2", "1:end:5", "0:end:6"])
