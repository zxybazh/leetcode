class Solution(object):
    def find(self, x):
        if x == self.fa[x]:
            return x
        else:
            self.fa[x] = self.find(self.fa[x])
            return self.fa[x]

    def findRedundantConnection(self, edges):
        """
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        self.fa = range(len(edges)+1)
        for e in edges:
            if self.find(e[0]) == self.find(e[1]):
                return e
            else:
                self.fa[self.find(e[0])] = self.find(e[1])



if __name__ == "__main__":
    print Solution().findRedundantConnection([[1,2], [1,3], [2,3]])
    print Solution().findRedundantConnection([[1,2], [2,3], [3,4], [1,4], [1,5]])
