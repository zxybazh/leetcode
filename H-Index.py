class Solution(object):
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        ans = 0
        citations = sorted(citations, reverse=True)
        for i, x in enumerate(citations):
            if x >= i+1:
                ans = i+1
        return ans


if __name__ == "__main__":
    print Solution().hIndex([3,0,6,1,5])
