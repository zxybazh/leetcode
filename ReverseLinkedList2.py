# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        if m == n: return head
        previous = None
        beforeFirstNode = None
        node = head
        for i in xrange(1, n+1):
            if i < m-1:
                node = node.next
            elif i == m-1:
                beforeFirstNode = node
                node = node.next
            else:
                temp = node.next
                if i == m:
                    firstNode = node
                    previous = node
                if i == n:
                    lastNode = node
                    firstNode.next = node.next
                    if previous != None:
                        node.next = previous
                else:
                    if previous != None:
                        node.next = previous
                    previous = node
                node = temp
        if beforeFirstNode != None:
            beforeFirstNode.next = lastNode
            return head
        else:
            return lastNode


