# Definition for singly-linked list.
class ListNode(object):
     def __init__(self, x):
         self.val = x
         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        t1 = head
        t2 = head
        while t1 is not None and t2 is not None:
            t1 = t1.next
            t2 = t2.next
            if t1 is None or t2 is None or t2.next is None:
                return False
            t2 = t2.next
            if t1 == t2:
                return True
        return False
