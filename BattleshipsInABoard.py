class Solution(object):
    def countBattleships(self, board):
        """
        :type board: List[List[str]]
        :rtype: int
        """
        ans = 0
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                if board[i][j] == 'X' and (i < 1 or board[i-1][j] != 'X') and (j < 1 or board[i][j-1] != 'X'):
                    ans += 1
        return ans


if __name__ == "__main__":
    print Solution().countBattleships("X..X\n...X\n...X".split("\n"))
