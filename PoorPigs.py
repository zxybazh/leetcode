import math

class Solution(object):
    def poorPigs(self, buckets, minutesToDie, minutesToTest):
        """
        :type buckets: int
        :type minutesToDie: int
        :type minutesToTest: int
        :rtype: int
        """
        return int(math.ceil(math.log(buckets, int(minutesToTest/minutesToDie)+1)))


if __name__ == "__main__":
    print Solution().poorPigs(1000, 15, 60)
    print Solution().poorPigs(1, 1, 1)
