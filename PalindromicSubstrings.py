class Solution(object):
    def countSubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        s = "@#"+"#".join(s)+"#$"
        maxr = 0
        pos = 0
        rl = [0 for i in s]
        ans = 0
        for i in xrange(len(s)):
            if i < maxr:
                rl[i] = min(rl[2*pos-i], maxr-i)
            else:
                rl[i] = 1
            while i >= rl[i] and i+rl[i] < len(s) and s[i-rl[i]] == s[i+rl[i]]:
                rl[i] += 1
            if rl[i]+i-1 > maxr:
                maxr = rl[i]+i-1
                pos = i
            ans += rl[i]/2
        return ans


if __name__ == "__main__":
    print Solution().countSubstrings("abc")
    print Solution().countSubstrings("aaa")
