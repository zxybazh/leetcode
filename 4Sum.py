class Solution(object):
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        ans = []
        nums = sorted(nums)
        for i, x in enumerate(nums):
            for j in xrange(i+1, len(nums)):
                y = nums[j]
                t = len(nums)-1
                for k in xrange(j+1, len(nums)):
                    while t > k+1 and x+y+nums[k] > target-nums[t]:
                        t -= 1
                    if t > k and x+y+nums[k]+nums[t] == target:
                        ans += [(x, y, nums[k], nums[t])]
        ans = [list(w) for w in set(ans)]
        return ans


if __name__ == "__main__":
    print Solution().fourSum([1, 0, -1, 0, -2, 2], target = 0)
    print Solution().fourSum([0, -1, 0, 0, 1, 0, 0], target=0)
    print Solution().fourSum([-5,-4,-3,-2,-1,0,0,1,2,3,4,5], 0)
