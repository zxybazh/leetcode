class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        count = {0:1}
        current = []
        ans = 0
        for i, x in enumerate(nums):
            if len(current) == 0: current.append(x)
            else: current.append(current[-1] + x)
            if count.has_key(current[-1] - k):
                ans += count[current[-1] -k]
            if count.has_key(current[-1]):
                count[current[-1]] += 1
            else:
                count[current[-1]] = 1
        return ans


if __name__ == "__main__":
    print Solution().subarraySum(nums=[1, 1, 1], k=2)
