class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        if dividend == 0: return 0
        if (dividend > 0 and divisor > 0) or (dividend < 0 and divisor < 0): sign = 1
        else: sign = -1
        divisor = abs(divisor)
        dividend = abs(dividend)
        res = 0
        l = [divisor]
        while l[-1] <= dividend: l.append(l[-1]+l[-1])
        for w in l[::-1]:
            if dividend >= w:
                res += res + 1
                dividend -= w
            else:
                res += res
        if sign == -1:
            res = -res
        if res == 2**31: res -= 1
        return res



if __name__ == "__main__":
    print Solution().divide(10, 3)
    print Solution().divide(7, -3)
    print Solution().divide(-6, -3)
    print Solution().divide(-7, -3)
    print Solution().divide(-2**31, -1)
