class Solution(object):
    def dfs(self, depth, sety, setxpy, setxmy):
        if depth == self.n:
            self.ans.append(self.board[:])
        else:
            for y in xrange(self.n):
                if not y in sety and not depth+y in setxpy and not depth-y in setxmy:
                    self.board[depth] = '.' * y + 'Q' + '.' * (self.n-y-1)
                    sety.add(y)
                    setxpy.add(depth+y)
                    setxmy.add(depth-y)
                    self.dfs(depth+1, sety, setxpy, setxmy)
                    sety.remove(y)
                    setxpy.remove(depth + y)
                    setxmy.remove(depth - y)
                    self.board[depth] = '.' * self.n

    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        self.n = n
        self.ans = []
        self.board = ['.' * n for i in xrange(n)]
        self.dfs(0, set(), set(), set())
        return self.ans

if __name__ == "__main__":
    print Solution().solveNQueens(8)
