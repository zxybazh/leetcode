class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        maxn = -1
        l = 0
        r = len(height)-1
        while l < r:
            maxn = max(maxn, min(height[r], height[l])*(r-l))
            if height[l] < height[r]:
                l += 1
            else:
                r -= 1
        return maxn


if __name__ == "__main__":
    print Solution().maxArea([1,8,6,2,5,4,8,3,7])
