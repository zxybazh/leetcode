# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def dfs(self, x):
        if x.left is None and x.right is None:
            return [0, x.val]
        else:
            res = [0, x.val]
            if x.left is not None:
                resL = self.dfs(x.left)
                res[0] += max(resL[0], resL[1])
                res[1] += resL[0]
            if x.right is not None:
                resR = self.dfs(x.right)
                res[0] += max(resR[0], resR[1])
                res[1] += resR[0]
            return res

    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None:
            return 0
        res = self.dfs(root)
        return max(res[0], res[1])
