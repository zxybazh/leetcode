class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        s = 0
        minS = 0
        ans = -1e10
        for w in nums:
            s += w
            ans = max(ans, s-minS)
            minS = min(minS, s)
        return ans


if __name__ == "__main__":
    print Solution().maxSubArray([-2,1,-3,4,-1,2,1,-5,4])
    print Solution().maxSubArray([-2,-1,-3,-4])
