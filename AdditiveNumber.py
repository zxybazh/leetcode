class Solution(object):
    def check(self, x, y, s):
        if x.startswith('0') and x != '0': return False
        if y.startswith('0') and y != '0': return False
        t = x+y
        x = int(x)
        y = int(y)
        while len(t) < len(s):
            z = x+y
            t += str(z)
            x = y
            y = z
        return t == s

    def isAdditiveNumber(self, num):
        """
        :type num: str
        :rtype: bool
        """
        for i in xrange(1, len(num)-1):
            for j in xrange(i+1, len(num)):
                if self.check(num[:i], num[i:j], num):
                    return True
        return False



if __name__ == "__main__":
    print Solution().isAdditiveNumber("112358")
    print Solution().isAdditiveNumber("199100199")
    print Solution().isAdditiveNumber("11111")
    print Solution().isAdditiveNumber("1023")
