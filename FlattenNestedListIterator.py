# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
class NestedInteger(object):
   def isInteger(self):
       """
       @return True if this NestedInteger holds a single integer, rather than a nested list.
       :rtype bool
       """

   def getInteger(self):
       """
       @return the single integer that this NestedInteger holds, if it holds a single integer
       Return None if this NestedInteger holds a nested list
       :rtype int
       """

   def getList(self):
       """
       @return the nested list that this NestedInteger holds, if it holds a nested list
       Return None if this NestedInteger holds a single integer
       :rtype List[NestedInteger]
       """

class NestedIterator(object):
    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.ans = [nestedList]
        self.move()

    def move(self):
        while self.ans != [] and (self.ans[-1] == [] or not self.ans[-1][0].isInteger()):
            while self.ans != [] and self.ans[-1] == []:
                self.ans = self.ans[:-1]
            if self.ans == []: break
            if self.ans[-1][0].isInteger(): continue
            res = self.ans[-1][0].getList()
            self.ans[-1] = self.ans[-1][1:]
            self.ans.append(res)

    def next(self):
        """
        :rtype: int
        """
        ans = self.ans[-1][0]
        self.ans[-1] = self.ans[-1][1:]
        self.move()
        return ans


    def hasNext(self):
        """
        :rtype: bool
        """
        return self.ans != []

# Your NestedIterator object will be instantiated and called as such:
nestedList = NestedInteger ()
i, v = NestedIterator(nestedList), []
while i.hasNext(): v.append(i.next())
