class Solution(object):
    def candy(self, ratings):
        """
        :type ratings: List[int]
        :rtype: int
        """
        ans = [0 for i in ratings]
        t = []
        for i, x in enumerate(ratings):
            t.append((i, x))
        t = sorted(t, key=lambda x:x[1])
        for w in t:
            w0 = ans[w[0]-1]+1 if w[0] > 0 and ratings[w[0]-1] < ratings[w[0]] else 1
            w1 = ans[w[0]+1]+1 if w[0] < len(ratings)-1 and ratings[w[0]+1] < ratings[w[0]] else 1
            ans[w[0]] = max(w0, w1)
        return sum(ans)


if __name__ == "__main__":
    print Solution().candy([1,0,2])
    print Solution().candy([1,2,2])
