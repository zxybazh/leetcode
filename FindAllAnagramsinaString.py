class Solution(object):
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """
        if len(s) <= len(p): return []
        zeroNum = 0
        cnt = [p.count(chr(ord('a')+i)) for i in xrange(26)]
        zeroNum = cnt.count(0)
        for i in xrange(len(p)):
            newc = ord(s[i])-ord('a')
            cnt[newc] -= 1
            if cnt[newc] == 0:
                zeroNum += 1
            elif cnt[newc] == -1:
                zeroNum -= 1
        if zeroNum == 26:
            ans = [0]
        else:
            ans = []
        for i in xrange(len(p), len(s)):
            oldc = ord(s[i - len(p)]) - ord('a')
            cnt[oldc] += 1
            if cnt[oldc] == 0:
                zeroNum += 1
            elif cnt[oldc] == 1:
                zeroNum -= 1
            newc = ord(s[i]) - ord('a')
            cnt[newc] -= 1
            if cnt[newc] == 0:
                zeroNum += 1
            elif cnt[newc] == -1:
                zeroNum -= 1
            if zeroNum == 26:
                ans.append(i-len(p)+1)
        return ans



if __name__ == "__main__":
    print Solution().findAnagrams(s="cbaebabacd", p= "abc")
    print Solution().findAnagrams(s="abab", p="ab")
