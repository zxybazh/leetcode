class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        nums = sorted(nums)
        for i in xrange(len(nums)-1):
            if nums[i] == nums[i+1]:
                return True
        return False


if __name__ == "__main__":
    print Solution().containsDuplicate([1,1,1,3,3,4,3,2,4,2])
    print Solution().containsDuplicate([1,2,3,4])
