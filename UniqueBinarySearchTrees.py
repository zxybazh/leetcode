class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        f = [1, 1]
        for i in xrange(2, n+1):
            res = 0
            for j in xrange(i):
                res += f[j] * f[i-j-1]
            f.append(res)
        return f[n]


if __name__ == "__main__":
    print Solution().numTrees(10)
