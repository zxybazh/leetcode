# Definition for an interval.
class Interval(object):
     def __init__(self, s=0, e=0):
         self.start = s
         self.end = e

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        l = []
        for w in intervals:
            l.append((w.start, 0))
            l.append((w.end, 1))
        l = sorted(l)
        ans = []
        currentStart = None
        currentThick = 0
        for w in l:
            if w[1] == 0:
                currentThick += 1
                if currentThick == 1:
                    currentStart = w[0]
            else:
                currentThick -= 1
                if currentThick == 0:
                    ans.append(Interval(currentStart, w[0]))
        return ans
