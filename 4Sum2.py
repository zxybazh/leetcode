class Solution(object):
    def fourSumCount(self, A, B, C, D):
        """
        :type A: List[int]
        :type B: List[int]
        :type C: List[int]
        :type D: List[int]
        :rtype: int
        """
        ss = {}
        for x in A:
            for y in B:
                if x+y in ss:
                    ss[x+y] += 1
                else:
                    ss[x+y] = 1
        ans = 0
        for x in C:
            for y in D:
                if -x-y in ss:
                    ans += ss[-x-y]
        return ans


if __name__ == "__main__":
    print Solution().fourSumCount(A = [ 1, 2], B = [-2,-1], C = [-1, 2], D = [ 0, 2])
