import random

class Solution(object):
    def __init__(self):
        self.dx = [0, 1, 0, -1]
        self.dy = [1, 0, -1, 0]

    def updateMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        if len(matrix) == 0: return []
        self.ans = [[1e9 for j in matrix[0]] for i in matrix]
        q = []
        for i in xrange(len(matrix)):
            for j in xrange(len(matrix[0])):
                if matrix[i][j] == 0:
                    q.append((i, j, 0))
                    self.ans[i][j] = 0
        f = 0
        while f < len(q):
            x, y, d = q[f]
            if d > self.ans[x][y]:
                q = q[1:]
                continue
            for fx in xrange(4):
                tx = x + self.dx[fx]
                ty = y + self.dy[fx]
                if 0 <= tx < len(matrix) and 0 <= ty < len(matrix[0]) and d+1 < self.ans[tx][ty]:
                    self.ans[tx][ty] = d+1
                    q.append((tx, ty, d+1))
            f+=1
        return self.ans

if __name__ == "__main__":
    #print Solution().updateMatrix([[0,0,0],[0,1,0],[0,0,0]])
    #print Solution().updateMatrix([[0,0,0],[0,1,0],[1,1,1]])
    print Solution().updateMatrix([[0, 1] for i in xrange(5000)])
    #print Solution().updateMatrix([[random.randint(0, 1) for i in xrange(10000)]])
