class Solution(object):
    def find(self, l, r):
        maxn = 0 # what is the most that can nums1 have on the left
        while l <= r:
            mid = (l+r)/2
            if mid == 0 or mid + len(self.nums2) == self.left or (self.nums1[mid-1] <= self.nums2[self.left-mid]):
                maxn = mid
                l = mid + 1
            else:
                r = mid - 1
        return maxn, self.left-maxn

    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        if len(nums1) > len(nums2):
            temp = nums2
            nums2 = nums1
            nums1 = temp
        if nums1 == []:
            if len(nums2) % 2 == 1:
                return nums2[len(nums2)/2]
            else:
                return (nums2[len(nums2)/2-1]+nums2[len(nums2)/2])/2.
        self.nums1 = nums1
        self.nums2 = nums2
        self.left = (len(nums1) + len(nums2) + 1)/2
        l1, l2 = self.find(max(0, self.left-len(nums2)), min(self.left, len(nums1)))

        if l1 > 0 and l2 > 0:
            ll = max(nums1[l1 - 1], nums2[l2 - 1])
        elif l1 > 0:
            ll = nums1[l1 - 1]
        else:
            ll = nums2[l2 - 1]
        if (len(nums1)+len(nums2)) % 2 == 0:
            if l1 < len(nums1) and l2 < len(nums2):
                rr = min(nums1[l1], nums2[l2])
            elif l1 < len(nums1):
                rr = nums1[l1]
            else:
                rr = nums2[l2]
            return (ll + rr) / 2.
        else:
            return ll

if __name__ == "__main__":
    print Solution().findMedianSortedArrays(nums1 = [1, 3], nums2 = [2])
    print Solution().findMedianSortedArrays(nums1 = [1, 2], nums2 = [3, 4])
    print Solution().findMedianSortedArrays(nums1 = [], nums2 = [2, 3])
