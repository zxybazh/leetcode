class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        d = {}
        for i,x in enumerate(nums):
            if d.has_key(x) and d[x]+k >= i:
                return True
            else:
                d[x] = i
        return False

if __name__ == "__main__":
    print Solution().containsNearbyDuplicate([1,2,3,1], k = 3)
    print Solution().containsNearbyDuplicate([1,0,1,1], k = 1)
    print Solution().containsNearbyDuplicate([1,2,3,1,2,3], k = 2)
