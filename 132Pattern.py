class Solution(object):
    def find132pattern(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        smallest = nums[:]
        for i, x in enumerate(smallest):
            if i > 0:
                smallest[i] = min(smallest[i-1], smallest[i])
        best = -1e9
        queue = []
        for i in xrange(len(nums)-1, 0, -1):
            while queue != [] and nums[i] > queue[-1]:
                if queue[-1] > best: best = queue[-1]
                queue = queue[:-1]
            queue.append(nums[i])
            if best != -1e9 and smallest[i-1] < best:
                return True
        return False


if __name__ == "__main__":
    print Solution().find132pattern([1, 2, 3, 4])
    print Solution().find132pattern([3, 1, 4, 2])
    print Solution().find132pattern([-1, 3, 2, 0])
    print Solution().find132pattern([1,0,1,-4,-3])
    print Solution().find132pattern([-2, 1, -1])
