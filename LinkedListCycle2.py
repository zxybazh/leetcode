# Definition for singly-linked list.
class ListNode(object):
     def __init__(self, x):
         self.val = x
         self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        t1 = head
        t2 = head
        while t1 is not None and t2 is not None:
            t1 = t1.next
            t2 = t2.next
            if t1 is None or t2 is None or t2.next is None:
                return None
            t2 = t2.next
            if t1 == t2:
                cnt = 1
                t1 = t1.next
                t2 = t2.next.next
                while t1 != t2:
                    t1 = t1.next
                    t2 = t2.next.next
                    cnt += 1
                h1 = head
                h2 = head
                for i in xrange(cnt):
                    h2 = h2.next
                while h1 != h2:
                    h1 = h1.next
                    h2 = h2.next
                return h1
        return None

if __name__ == "__main__":
    a = ListNode(1)
    b = ListNode(2)
    a.next = b
    b.next = a
    print Solution().detectCycle(a)
