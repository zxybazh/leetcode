class Solution(object):
    def __init__(self):
        self.dx = [0, 1, 0, -1]
        self.dy = [1, 0, -1, 0]

    def search(self, x, y, current):
        current += self.board[x][y]
        self.board[x][y] = "."
        if current.__len__() == self.word.__len__(): return True
        for direction in xrange(4):
            tx = x + self.dx[direction]
            ty = y + self.dy[direction]
            if 0 <= tx < len(self.board) and 0 <= ty < len(self.board[0]) and \
                    self.board[tx][ty] == self.word[len(current)]:
                    if self.search(tx, ty, current):
                        return True
        self.board[x][y] = current[-1]
        return False

    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        self.board = board
        self.word = word
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                if board[i][j] == word[0] and self.search(i, j, ""):
                    return True
        return False



if __name__ == "__main__":
    print Solution().exist(board =
    [
      ['A','B','C','E'],
      ['S','F','C','S'],
      ['A','D','E','E']
    ], word = "ABCCED")
    print Solution().exist(board=
    [
        ['A', 'B', 'C', 'E'],
        ['S', 'F', 'C', 'S'],
        ['A', 'D', 'E', 'E']
    ], word="SEE")
    print Solution().exist(board=
    [
        ['A', 'B', 'C', 'E'],
        ['S', 'F', 'C', 'S'],
        ['A', 'D', 'E', 'E']
    ], word="ABCB")
