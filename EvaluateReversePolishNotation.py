class Solution(object):
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        stack = []
        for token in tokens:
            if token not in "+-/*":
                stack.append(int(token))
            else:
                tmp = int(eval(str(stack[-2]) + ".0 " + token + " " + str(stack[-1])))
                #print str(stack[-2]) + " " + token + " " + str(stack[-1])
                stack = stack[:-1]
                stack[-1] = tmp
        return stack[-1]


if __name__ == "__main__":
    print Solution().evalRPN(["2", "1", "+", "3", "*"])
    print Solution().evalRPN(["4", "13", "5", "/", "+"])
    print Solution().evalRPN(["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"])
