# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def findBottomLeftValue(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        ans = []
        queue = [(root, 0)]
        while len(queue) > 0:
            qf = queue[0]
            queue = queue[1:]
            if qf[1] == len(ans):
                ans.append(qf[0].val)
            if qf[0].left is not None:
                queue.append((qf[0].left, qf[1]+1))
            if qf[0].right is not None:
                queue.append((qf[0].right, qf[1]+1))
        return ans[-1]
