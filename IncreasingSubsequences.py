class Solution(object):
    def findSubsequences(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        ans = []
        for w in nums:
            l = len(ans)
            for t in ans[:l]:
                if w >= t[-1]:
                    ans.append(t[:]+[w])
            ans.append([w])
            ans = set(['['+','.join(str(t) for t in w)+']' for w in ans])
            ans = [eval(w) for w in ans]
        return filter(lambda x:len(x) > 1, ans)


if __name__ == "__main__":
    print Solution().findSubsequences([4, 6, 7, 7])
