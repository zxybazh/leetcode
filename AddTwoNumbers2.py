# Definition for singly-linked list.
class ListNode(object):
     def __init__(self, x):
         self.val = x
         self.next = None

class Solution(object):
    def doit(self, l1, l2, c1, c2):
        if c1 > c2:
            res = self.doit(l1.next, l2, c1-1, c2)
            cur = ListNode(l1.val)
        elif c2 > c1:
            res = self.doit(l1, l2.next, c1, c2-1)
            cur = ListNode(l2.val)
        else:
            if c1 == 1 and c2 == 1:
                return ListNode(l1.val + l2.val)
            else:
                res = self.doit(l1.next, l2.next, c1-1, c2-1)
                cur = ListNode(l1.val + l2.val)
        cur.next = res
        if res.val > 9:
            cur.val += res.val / 10
            res.val %= 10
        return cur

    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        t = l1
        cnt1 = 0
        while t!= None:
            cnt1 += 1
            t = t.next
        t = l2
        cnt2 = 0
        while t!= None:
            cnt2 += 1
            t = t.next
        res = self.doit(l1, l2, cnt1, cnt2)
        if res.val > 9:
            cur = ListNode(res.val / 10)
            res.val %= 10
            cur.next = res
            return cur
        else:
            return res
