class Trie(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.c = [None for i in xrange(26)]
        self.flag = False

    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        if len(word) == 0:
            self.flag = True
        else:
            xx = ord(word[0])-ord('a')
            if self.c[xx] is None:
                self.c[xx] = Trie()
            self.c[xx].insert(word[1:])

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        if len(word) == 0:
            return self.flag
        else:
            xx = ord(word[0]) - ord('a')
            if self.c[xx] is None:
                return False
            else:
                return self.c[xx].search(word[1:])

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        if len(prefix) == 0:
            return True
        else:
            xx = ord(prefix[0]) - ord('a')
            if self.c[xx] is None:
                return False
            else:
                return self.c[xx].startsWith(prefix[1:])


if __name__ == "__main__":
    # Your Trie object will be instantiated and called as such:
    obj = Trie()
    obj.insert("apple")
    print obj.search("app")
    print obj.startsWith("app")
