class Solution(object):
    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        ans = []
        t = []
        for i, s in enumerate(words):
            t.append((s, 0, i))
            t.append((s[::-1], 1, i))
        t = sorted(t)
        for i in xrange(len(t)):
            for j in xrange(i+1, len(t)):
                l = min(len(t[i][0]), len(t[j][0]))
                if t[i][0][:l] != t[j][0][:l]: break
                if t[i][1] == t[j][1] or t[i][2] == t[j][2]: continue
                if l == len(t[i][0]):
                    x = t[j][0][l:]
                else:
                    x = t[i][0][l:]
                if x[::-1] == x:
                    if t[i][1] == 0:
                        ans.append([t[i][2], t[j][2]])
                    else:
                        ans.append([t[j][2], t[i][2]])
        return ans


if __name__ == "__main__":
    print Solution().palindromePairs(["abcd","dcba","lls","s","sssll"])
    print Solution().palindromePairs(["bat","tab","cat"])
    print Solution().palindromePairs(["a", ""])
