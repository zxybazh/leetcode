class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        dic = {}
        chs = []
        for w in s+t:
            if w not in dic:
                dic[w] = len(chs)
                chs.append(w)
        s = [dic[c] for c in s]
        t = [dic[c] for c in t]
        d = [0 for i in xrange(len(chs))]
        c = [0 for i in xrange(len(chs))]
        for w in t: d[w] += 1
        f = 0
        ans = None
        for i in xrange(len(s)):
            c[s[i]] += 1
            while f < i and c[s[f]] > d[s[f]]:
                c[s[f]] -= 1
                f += 1
            flag = True
            for t in xrange(len(chs)):
                if c[t] < d[t]:
                    flag = False
                    break
            if flag and (ans is None or i-f+1 < ans[1]-ans[0]):
                ans = (f, i+1)
        if ans is None: return ""
        else: return ''.join([chs[w] for w in s[ans[0]:ans[1]]])


if __name__ == "__main__":
    print Solution().minWindow(s = "ADOBECODEBANC", t = "ABC")
    print Solution().minWindow('a', 'aa')
