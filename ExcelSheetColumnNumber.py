class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        d = 1
        ans = 0
        for w in s[::-1]:
            ans += d*(ord(w)-ord('A')+1)
            d *= 26
        return ans


if __name__ == "__main__":
    print Solution().titleToNumber("A")
