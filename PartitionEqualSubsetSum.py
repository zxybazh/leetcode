class Solution(object):
    def canPartition(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        total = sum(nums)
        f = [False for j in xrange(total + 1)]
        f[0] = True
        for x in nums:
            for j in xrange(total, x-1, -1):
                f[j] |= f[j-x]
        return total % 2 == 0 and f[total / 2]


if __name__ == "__main__":
    print Solution().canPartition([1, 5, 11, 5])
    print Solution().canPartition([1, 2, 3, 5])
