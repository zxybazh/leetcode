class Solution(object):
    def process(self, x):
        return " ".join([w[0].upper()+w[1:] for w in x.split(" ")])

    def numberToWords(self, num):
        """
        :type num: int
        :rtype: str
        """
        self.n = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90,
                  100, 1000, 10**6, 10**9]
        self.s = self.process('zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen ' +
                              'fifteen sixteen seventeen eighteen nineteen twenty thirty forty fifty sixty seventy ' +
                              'eighty ninety hundred thousand million billion').split(" ")
        if num >= 10 ** 9:
            return self.numberToWords(num / 10 ** 9) + " " + self.s[self.n.index(10 ** 9)]\
                   + (" " + self.numberToWords(num % 10 ** 9) if num % 10 ** 9 > 0 else "")
        if num >= 10 ** 6:
            return self.numberToWords(num / 10 ** 6) + " " + self.s[self.n.index(10 ** 6)] \
                   + (" " + self.numberToWords(num % 10 ** 6) if num % 10 ** 6 > 0 else "")
        if num >= 10 ** 3:
            return self.numberToWords(num / 10 ** 3) + " " + self.s[self.n.index(10 ** 3)] \
                   + (" " + self.numberToWords(num % 10 ** 3) if num % 10 ** 3 > 0 else "")
        if num >= 10 ** 2:
            return self.numberToWords(num / 10 ** 2) + " " + self.s[self.n.index(10 ** 2)] \
                   + (" " + self.numberToWords(num % 10 ** 2) if num % 10 ** 2 > 0 else "")
        if num <= 20:
            return self.s[self.n.index(num)]
        return self.s[self.n.index(num / 10 * 10)] + (" " + self.numberToWords(num % 10) if num % 10 > 0 else "")


if __name__ == "__main__":
    print Solution().numberToWords(1234567891)
    print Solution().numberToWords(1234567)
    print Solution().numberToWords(12345)
    print Solution().numberToWords(123)
    print Solution().numberToWords(1)
    print Solution().numberToWords(200)


