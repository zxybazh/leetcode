class Solution(object):
    def topKFrequent(self, words, k):
        """
        :type words: List[str]
        :type k: int
        :rtype: List[str]
        """
        t = {}
        for w in words:
            if w in t:
                t[w] += 1
            else:
                t[w] = 0
        return [c[1] for c in sorted([(-t[w], w) for w in t])[:k]]


if __name__ == "__main__":
    print Solution().topKFrequent(["i", "love", "leetcode", "i", "love", "coding"], k = 2)
    print Solution().topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4)
