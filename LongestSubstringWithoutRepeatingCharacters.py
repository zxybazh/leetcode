class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        ans = 0
        queue = []
        for i in xrange(len(s)):
            while s[i] in queue:
                queue = queue[queue.index(s[i])+1:]
            queue.append(s[i])
            ans = max(ans, len(queue))
        return ans


if __name__ == "__main__":
    print Solution().lengthOfLongestSubstring("abcabcbb")
    print Solution().lengthOfLongestSubstring("bbbbb")
    print Solution().lengthOfLongestSubstring("pwwkew")
