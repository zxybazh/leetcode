import hashlib

class Codec:
    def __init__(self):
        self.lib = {}

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.

        :type longUrl: str
        :rtype: str
        """
        res = hashlib.sha256(longUrl).hexdigest()
        self.lib[res] = longUrl
        return res


    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.

        :type shortUrl: str
        :rtype: str
        """
        return self.lib[shortUrl]

# Your Codec object will be instantiated and called as such:
if __name__ == "__main__":
    codec = Codec()
    print codec.encode("Hello")
    print codec.decode(codec.encode("Hello"))
