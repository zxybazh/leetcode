class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        x = 0
        for w in nums: x ^= w
        return x

if __name__ == "__main__":
    print Solution().singleNumber([2,2,1])
    print Solution().singleNumber([4,1,2,1,2])
