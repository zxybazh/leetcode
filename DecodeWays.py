class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        f = [0 for i in xrange(len(s)+1)]
        f[0] = 1
        for i, x in enumerate(s):
            if s[i] != '0':
                f[i+1] += f[i]
            if 0 < i < len(s) and int(s[i-1:i+1]) <= 26 and s[i-1] != '0':
                f[i+1] += f[i-1]
        return f[len(s)]


if __name__ == "__main__":
    print Solution().numDecodings("12")
    print Solution().numDecodings("226")
    print Solution().numDecodings("0")
    print Solution().numDecodings("10")
