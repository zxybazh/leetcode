class Solution(object):
    def dfs(self, x, k):
        for i in xrange(k):
            if x + str(i) not in self.visited:
                self.visited.add(x+str(i))
                if x != "": self.dfs(x[1:]+str(i), k)
                self.ans += str(i)
                #print self.ans, self.visited

    def crackSafe(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: str
        """
        self.ans = ""
        self.visited = set()
        self.dfs("0"*(n-1), k)
        return self.ans+"0"*(n-1)




if __name__ == "__main__":
    print Solution().crackSafe(1, 1)
    print Solution().crackSafe(2, 2)
    print Solution().crackSafe(3, 2)
