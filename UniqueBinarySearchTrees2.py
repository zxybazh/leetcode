# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def dfs(self, s):
        if len(s) == 0: return [None]
        if len(s) == 1:
            return [TreeNode(s[0])]
        else:
            res = []
            for w in s:
                resl = self.dfs(filter(lambda x: x < w, s))
                resr = self.dfs(filter(lambda x: x > w, s))
                for ll in resl:
                    for rr in resr:
                        x = TreeNode(w)
                        x.left = ll
                        x.right = rr
                        res.append(x)
            return res


    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        if n == 0: return []
        self.ans = self.dfs(range(1, n+1))
        return self.ans


def treePrint(x):
    queue = [x]
    print x.val,
    while len(queue) > 0:
        x = queue[0]
        queue = queue[1:]
        if x.left is not None or x.right is not None:
            if x.left is None:
                print x.left,
            else:
                print x.left.val,
                queue.append(x.left)
            if x.right is None:
                print x.right,
            else:
                print x.right.val,
                queue.append(x.right)
    print

if __name__ == "__main__":
    for t in Solution().generateTrees(3):
        treePrint(t)
