class Solution(object):
    def repeatedSubstringPattern(self, s):
        """
        :type s: str
        :rtype: bool
        """
        for i in xrange(1, len(s)):
            if s[:i]*(len(s)/i) == s:
                return True
        return False


if __name__ == "__main__":
    print Solution().repeatedSubstringPattern("abab")
    print Solution().repeatedSubstringPattern("abcabcabc")
    print Solution().repeatedSubstringPattern("aba")
