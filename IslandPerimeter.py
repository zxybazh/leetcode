class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        ans = sum([sum(w) for w in grid]) * 4
        for i in xrange(len(grid)):
            for j in xrange(len(grid[0])):
                if grid[i][j] == 0: continue
                if j+1 < len(grid[0]) and grid[i][j+1] == 1:
                    ans -= 2
                if i+1 < len(grid) and grid[i+1][j] == 1:
                    ans -= 2
        return ans

if __name__ == "__main__":
    print Solution().islandPerimeter([[0,1,0,0],
         [1,1,1,0],
         [0,1,0,0],
         [1,1,0,0]])
