class Solution(object):
    def validPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        l = 0
        r = len(s)-1
        while r > l and s[l] == s[r]:
            l += 1
            r -= 1
        if r-l < 1: return True
        ll = l
        rr = r
        l += 1
        while r > l and s[l] == s[r]:
            l += 1
            r -= 1
        if r-l < 1: return True
        l = ll
        r = rr-1
        while r > l and s[l] == s[r]:
            l += 1
            r -= 1
        if r-l < 1: return True
        return False

if __name__ == "__main__":
    print Solution().validPalindrome('abca')
    print Solution().validPalindrome('aba')
    print Solution().validPalindrome('abc')
