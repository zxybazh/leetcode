class Solution(object):
    def isPowerOfThree(self, n):
        """
        :type n: int
        :rtype: bool
        """
        ans = 0
        l = 1
        r = 20
        while l <= r:
            mid = (l + r) / 2
            if 3 ** mid <= n:
                ans = mid
                l = mid + 1
            else:
                r = mid - 1
        return 3 ** ans == n

if __name__ == "__main__":
    print Solution().isPowerOfThree(0)
    print Solution().isPowerOfThree(9)
    print Solution().isPowerOfThree(27)
    print Solution().isPowerOfThree(45)
