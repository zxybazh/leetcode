class Solution(object):
    def dfs(self, x, fa):
        self.dist[x] = self.dist[fa][:] + [x]
        for y in self.edges[x]:
            if y == fa: continue
            self.dfs(y, x)

    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        self.edges = [[] for i in xrange(n)]
        for e in edges:
            self.edges[e[0]].append(e[1])
            self.edges[e[1]].append(e[0])
        self.dist = [[] for i in xrange(n)]
        self.dfs(0, -1)
        c = max(self.dist, key=lambda x:len(x))[-1]
        self.dist = [[] for i in xrange(n)]
        self.dfs(c, -1)
        dist = max(self.dist, key=lambda x:len(x))
        return dist[(len(dist)-1)/2:(len(dist)+2)/2]

if __name__ == "__main__":
    print Solution().findMinHeightTrees(n = 4, edges = [[1, 0], [1, 2], [1, 3]])
    print Solution().findMinHeightTrees(n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]])
