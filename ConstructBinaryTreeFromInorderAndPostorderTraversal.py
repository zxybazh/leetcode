# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    def buildTree(self, inorder, postorder):
        """
        :type inorder: List[int]
        :type postorder: List[int]
        :rtype: TreeNode
        """
        if len(postorder) == 0:
            return None
        rt = TreeNode(postorder[-1])
        if len(postorder) > 1:
            rt.left = self.buildTree(inorder[:inorder.index(rt.val)], postorder[:inorder.index(rt.val)])
            rt.right = self.buildTree(inorder[inorder.index(rt.val)+1:], postorder[inorder.index(rt.val):-1])
        return rt
