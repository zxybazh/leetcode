class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        n = len(gas)
        for i, x in enumerate(gas): gas[i] -= cost[i]
        t = [0]
        for i, x in enumerate(gas):
            t.append(t[-1] + gas[i])
        t = t[1:]
        minX = min(t)
        if minX >= 0: return 0
        queue = []
        for i, w in enumerate(t):
            while len(queue) > 0 and queue[-1][1] >= w:
                queue = queue[:-1]
            queue.append((i, w))
        for i in xrange(n): t.append(t[-1]+gas[i])
        delta = 0
        for i in xrange(1, n):
            delta += gas[i-1]
            while len(queue) > 0 and queue[0][0] < i:
                queue = queue[1:]
            while len(queue) > 0 and queue[-1][1] >= t[i+n-1]:
                queue = queue[:-1]
            queue.append((i, t[i+n-1]))
            minX = queue[0][1]
            if minX-delta >= 0:
                return i
        return -1


if __name__ == "__main__":
    print Solution().canCompleteCircuit(gas  = [1,2,3,4,5], cost = [3,4,5,1,2])
    print Solution().canCompleteCircuit(gas  = [2,3,4], cost = [3,4,3])
