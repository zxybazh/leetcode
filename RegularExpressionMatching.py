class Solution(object):

    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        #print '|' + s + '|', '|' + p + '|'
        #for i in xrange(10000): s += ']'
        #for i in xrange(10000): s = s[:-1]
        if s == p: return True
        if p == '': return False
        if s == '':
            if len(p) % 2 == 1: return False
            for i in xrange(len(p) / 2):
                if p[i*2+1] != '*':
                    return False
            return True
        if len(p) > 1 and p[1] == '*':
            while len(p) > 3 and p[3] == '*' and (p[0] == p[2] or p[0] == '.' or p[2] == '.'):
                if p[0] == '.': p = p[:2]+'.'+p[3:]
                p = p[2:]
            if self.isMatch(s, p[2:]): return True
            for i in xrange(len(s)):
                if p[0] == '.' or p[0] == s[i]:
                    if self.isMatch(s[i+1:], p[2:]):
                        return True
                else:
                    return self.isMatch(s[i:], p[2:])
            return False
        else:
            return (s[0] == p[0] or p[0] == '.') and self.isMatch(s[1:], p[1:])


if __name__ == "__main__":
    print Solution().isMatch("aa", "a")
    print Solution().isMatch("aa", "a*")
    print Solution().isMatch("ab", ".*")
    print Solution().isMatch("aab", "c*a*b")
    print Solution().isMatch("mississippi", "mis*is*p*.")
    print Solution().isMatch("mississippi", "mis*is*ip*.")
    print Solution().isMatch("ab", ".*c")
    print Solution().isMatch("bbbba", ".*a*a")
    print Solution().isMatch("aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*c")
