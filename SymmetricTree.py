# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def equal(self, x, y):
        if x is None and y is None:
            return True
        elif x is None or y is None:
            return False
        return x.val == y.val and self.equal(x.left, y.right) and self.equal(x.right, y.left)

    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root is None: return True
        return self.equal(root.left, root.right)
