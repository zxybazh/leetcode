class Solution(object):
    def find(self, x):
        if self.fa[x] == x:
            return x
        else:
            tfa = self.find(self.fa[x])
            self.vl[x] *= self.vl[self.fa[x]]
            self.fa[x] = tfa
            return tfa

    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        self.fa = [i for i in range(len(equations)*2)]
        self.vl = [1 for i in range(len(equations)*2)]
        self.ss = {}
        for i, p in enumerate(equations):
            res = values[i]
            x = [0, 0]
            for t in range(2):
                if self.ss.has_key(p[t]):
                    x[t] = self.ss[p[t]]
                else:
                    x[t] = len(self.ss)
                    self.ss[p[t]] = x[t]
            if self.find(x[0]) == self.find(x[1]):
                continue
            fa = [self.find(x[0]), self.find(x[1])]
            self.fa[fa[0]] = fa[1]
            self.vl[fa[0]] = res / self.vl[x[0]] * self.vl[x[1]]
        ans = []
        for p in queries:
            if not self.ss.has_key(p[0]) or not self.ss.has_key(p[1]) or self.find(self.ss[p[0]]) != self.find(self.ss[p[1]]):
                ans.append(-1.0)
            else:
                ans.append(self.vl[self.ss[p[0]]]/self.vl[self.ss[p[1]]])
        return ans


if __name__ == "__main__":
    print Solution().calcEquation(equations = [ ["a", "b"], ["b", "c"] ], values = [2.0, 3.0], queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ])
