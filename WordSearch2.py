class Solution(object):
    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """
        ans = []
        self.board = board
        words = sorted(words)
        last = ""
        for word in words:
            if word == last: continue
            last = word
            if self.exist(board, word):
                ans.append(word)
        return ans

    def exist(self, board, word):
        word = word[::-1]
        self.word = word
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                if board[i][j] == word[0] and self.search(i, j, ""):
                    return True
        return False

    def __init__(self):
        self.dx = [0, 1, 0, -1]
        self.dy = [1, 0, -1, 0]

    def search(self, x, y, current):
        current += self.board[x][y]
        if current.__len__() == self.word.__len__(): return True
        self.board[x][y] = "."
        for direction in xrange(4):
            tx = x + self.dx[direction]
            ty = y + self.dy[direction]
            if 0 <= tx < len(self.board) and 0 <= ty < len(self.board[0]) and \
                    self.board[tx][ty] == self.word[len(current)]:
                if self.search(tx, ty, current):
                    self.board[x][y] = current[-1]
                    return True
        self.board[x][y] = current[-1]
        return False



if __name__ == "__main__":
    print Solution().findWords(board =
    [
        ['o', 'a', 'a', 'n'],
        ['e', 't', 'a', 'e'],
        ['i', 'h', 'k', 'r'],
        ['i', 'f', 'l', 'v']
    ], words = ["oath","pea","eat","rain"])
    print Solution().findWords(board=
    [['a']], words=["a", "a"])
