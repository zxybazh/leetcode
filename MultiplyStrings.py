class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        a = []
        b = []
        for i, x in enumerate(num1):
            a.append(ord(x)-ord('0'))
        a = a[::-1]
        for i, x in enumerate(num2):
            b.append(ord(x)-ord('0'))
        b = b[::-1]
        c = [0 for i in xrange(len(a)+len(b)+1)]
        for i, x in enumerate(a):
            for j, y in enumerate(b):
                c[i+j] += x*y
        for i in xrange(len(a)+len(b)):
            if c[i] > 9:
                c[i+1] += c[i] / 10
                c[i] %= 10
        num3 = ''
        for i in c[::-1]:
            if num3 == '' and i == 0: continue
            num3 += chr(ord('0')+i)
        if num3 == '':
            num3 = '0'
        return num3

if __name__ == "__main__":
    print Solution().multiply(num1 = "2", num2 = "3")
    print Solution().multiply(num1 = "123", num2 = "456")
