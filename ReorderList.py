# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def reorderList(self, head):
        """
        :type head: ListNode
        :rtype: void Do not return anything, modify head in-place instead.
        """
        if head == None: return
        all = []
        start = head
        head = head.next
        while head is not None:
            all.append(head)
            head = head.next
        for i in xrange(len(all)/2):
            start.next = all[len(all) - i - 1]
            start.next.next = all[i]
            start = all[i]
        if len(all) % 2 == 0:
            start.next = None
        else:
            start.next = all[len(all)/2]
            all[len(all)/2].next = None


if __name__ == "__main__":
    t0 = ListNode(0)
    t1 = ListNode(0)
    t2 = ListNode(0)
    t3 = ListNode(0)
    t0.next = t1
    t1.next = t2
    t2.next = t3
    print Solution().reorderList(t0)
