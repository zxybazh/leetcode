class Solution(object):
    def dfs(self, d, s):
        print d, s
        if s == []:
            self.ans += 1
        else:
            for i in xrange(len(s)):
                if d % s[i] == 0 or s[i] % d == 0:
                    self.dfs(d-1, s[:i]+s[i+1:])


    def countArrangement(self, N):
        """
        :type N: int
        :rtype: int
        """
        self.ans = 0
        self.dfs(N, range(1, N+1))
        return self.ans


if __name__ == "__main__":
    print Solution().countArrangement(2)
    print Solution().countArrangement(15)
