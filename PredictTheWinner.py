class Solution(object):
    def search(self, nums):
        if len(nums) == 2:
            return [max(nums), min(nums)]
        if len(nums) == 1:
            return [nums[0], 0]
        else:
            res0 = self.search(nums[1:])
            res0[0], res0[1] = res0[1], res0[0]
            res0[0] += nums[0]

            res1 = self.search(nums[:-1])
            res1[0], res1[1] = res1[1], res1[0]
            res1[0] += nums[-1]

            if res0[0] > res1[0]:
                return res0
            else:
                return res1


    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) % 2 == 1:
            res = self.search(nums)
            return res[0] >= res[1]
        else:
            return True


if __name__ == "__main__":
    print Solution().PredictTheWinner([1, 5, 2])
    print Solution().PredictTheWinner([1, 5, 233, 7])
    print Solution().PredictTheWinner([0,0,7,6,5,6,1])
