class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        if endWord not in wordList: return 0
        if beginWord in wordList:
            wordList = wordList[:wordList.index(beginWord)]+wordList[wordList.index(beginWord)+1:]
        wordList = [beginWord] + wordList
        lookupTable = [{} for w in beginWord]
        for i in xrange(len(wordList)):
            for j in xrange(len(wordList[i])):
                t = wordList[i][:j] + wordList[i][j+1:]
                if lookupTable[j].has_key(t):
                    lookupTable[j][t].append(i)
                else:
                    lookupTable[j][t] = [i]
        queue = [0]
        dist = [1e9 for w in wordList]
        dist[0] = 0
        while len(queue) > 0:
            cur = queue[0]
            queue = queue[1:]
            for j in xrange(len(beginWord)):
                t = wordList[cur][:j] + wordList[cur][j+1:]
                if lookupTable[j].has_key(t):
                    for k in lookupTable[j][t]:
                        if k != cur and dist[k] > dist[cur] + 1:
                            dist[k] = dist[cur] + 1
                            queue.append(k)
        if dist[wordList.index(endWord)] == 1e9: return 0
        else: return dist[wordList.index(endWord)] + 1

if __name__ == "__main__":
    print Solution().ladderLength(
        beginWord = "hit",
        endWord = "cog",
        wordList = ["hot","dot","dog","lot","log","cog"])
    print Solution().ladderLength(
        beginWord = "hit",
        endWord = "cog",
        wordList = ["hot","dot","dog","lot","log"])
