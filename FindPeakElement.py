class Solution(object):
    def search(self, nums, l, r):
        if l == r: return l
        if nums[l+1] < nums[l]:
            return l
        if nums[r-1] < nums[r]:
            return r
        mid = (l+r)/2
        if nums[mid+1] > nums[mid]:
            return self.search(nums, mid+1, r)
        else:
            return self.search(nums, l, mid)

    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        return self.search(nums, 0, len(nums)-1)

if __name__ == "__main__":
    print Solution().findPeakElement([1,2,3,1])
    print Solution().findPeakElement([1,2,1,3,5,6,4])
