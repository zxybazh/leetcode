# Definition for a binary tree node.
class TreeNode(object):
     def __init__(self, x):
         self.val = x
         self.left = None
         self.right = None

class Solution(object):
    def dfs(self, cur):
        if cur.left is None and cur.right is None:
            return [cur.val, cur.val]
        elif cur.left is None:
            right = self.dfs(cur.right)
            return [max(cur.val, cur.val + right[0]),
                    max(cur.val, cur.val + right[0], right[1])]
        elif cur.right is None:
            left = self.dfs(cur.left)
            return [max(cur.val, cur.val + left[0]),
                    max(cur.val, cur.val + left[0], left[1])]
        else:
            left = self.dfs(cur.left)
            right = self.dfs(cur.right)
            return [max(cur.val, left[0] + cur.val, right[0] + cur.val),
                    max(cur.val, left[0] + cur.val, right[0] + cur.val, left[0] + cur.val + right[0], left[1], right[1])]

    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        return self.dfs(root)[1]


