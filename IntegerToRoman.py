class Solution(object):
    def process(self, x, s):
        if x == 9:
            return s[0]+s[2]
        if x == 4:
            return s[0]+s[1]
        if x < 4:
            return s[0]*x
        return s[1]+s[0]*(x-5)

    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        ans = ''
        self.s = 'IVXLCDM'
        self.v = [1, 5, 10, 50, 100, 500, 1000]
        while num >= 1000:
            ans += 'M'
            num -= 1000
        ans += self.process(num / 100 % 10, self.s[-3:])
        ans += self.process(num / 10  % 10, self.s[-5:-2])
        ans += self.process(num / 1   % 10, self.s[-7:-4])
        return ans


if __name__ == "__main__":
    print Solution().intToRoman(3)
    print Solution().intToRoman(4)
    print Solution().intToRoman(9)
    print Solution().intToRoman(58)
    print Solution().intToRoman(1994)
