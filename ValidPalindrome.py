class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        s = s.lower()
        while len(s) > 1:
            x = s[0]
            y = s[len(s)-1]
            if ('0' <= x <= '9' or 'a' <= x <= 'z') and ('0' <= y <= '9' or 'a' <= y <= 'z'):
                if x == y:
                    s = s[1:-1]
                else:
                    return False
            else:
                if not ('0' <= x <= '9' or 'a' <= x <= 'z'): s = s[1:]
                if not ('0' <= y <= '9' or 'a' <= y <= 'z'): s = s[:-1]
        return True


if __name__ == "__main__":
    print Solution().isPalindrome("A man, a plan, a canal: Panama")
    print Solution().isPalindrome("race a car")
