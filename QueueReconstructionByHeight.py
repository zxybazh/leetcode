class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """
        people = [[-w[0], w[1]] for w in sorted([(-w[0], w[1]) for w in people])]
        ans = []
        for w in people:
            ans = ans[:w[1]]+[w]+ans[w[1]:]
        return ans

if __name__ == "__main__":
    print Solution().reconstructQueue([[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]])
