class Solution(object):
    def checkInclusion(self, s1, s2):
        """
        :type s1: str
        :type s2: str
        :rtype: bool
        """
        s = [0 for i in xrange(26)]
        for w in s1:
            s[ord(w)-ord('a')] += 1
        for w in s2[:len(s1)-1]:
            s[ord(w)-ord('a')] -= 1
        cnt = 0
        for w in s:
            if w > 0:
                cnt += 1
        for i in xrange(len(s1)-1, len(s2)):
            s[ord(s2[i])-ord('a')] -= 1
            if s[ord(s2[i])-ord('a')] == 0:
                cnt -= 1
            if i >= len(s1):
                s[ord(s2[i-len(s1)]) - ord('a')] += 1
                if s[ord(s2[i-len(s1)]) - ord('a')] == 1:
                    cnt += 1
            if cnt == 0:
                return True
        return False

if __name__ == "__main__":
    print Solution().checkInclusion(s1 = "ab", s2 = "eidbaooo")
    print Solution().checkInclusion(s1 = "ab", s2 = "eidboaoo")
