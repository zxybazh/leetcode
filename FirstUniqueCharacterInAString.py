class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        count = [s.count(chr(ord('a')+i)) for i in xrange(26)]
        for i, w in enumerate(s):
            if count[ord(w)-ord('a')] == 1:
                return i
        return -1


if __name__ == "__main__":
    print Solution().firstUniqChar("leetcode")
    print Solution().firstUniqChar("loveleetcode")
