class Solution(object):
    def dfs(self, cur):
        if self.fromWord[cur] == []: return [[self.wordList[0]]]
        else:
            ans = []
            for w in self.fromWord[cur]:
                t = self.dfs(w)
                for l in t:
                    ans.append(l + [self.wordList[cur]])
            return ans

    def findLadders(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: List[List[str]]
        """
        if endWord not in wordList: return []
        if beginWord in wordList:
            wordList = wordList[:wordList.index(beginWord)]+wordList[wordList.index(beginWord)+1:]
        wordList = [beginWord] + wordList
        self.wordList = wordList
        lookupTable = [{} for w in beginWord]
        for i in xrange(len(wordList)):
            for j in xrange(len(wordList[i])):
                t = wordList[i][:j] + wordList[i][j+1:]
                if lookupTable[j].has_key(t):
                    lookupTable[j][t].append(i)
                else:
                    lookupTable[j][t] = [i]
        queue = [0]
        self.fromWord = [[] for w in wordList]
        f = 0
        r = 0
        dist = [1e9 for w in wordList]
        dist[0] = 0
        while f <= r:
            cur = queue[f]
            for j in xrange(len(beginWord)):
                t = wordList[cur][:j] + wordList[cur][j+1:]
                if lookupTable[j].has_key(t):
                    for k in lookupTable[j][t]:
                        if k != cur and dist[k] > dist[cur] + 1:
                            dist[k] = dist[cur] + 1
                            self.fromWord[k] = [cur]
                            queue.append(k)
                            r += 1
                        if k != cur and cur not in self.fromWord[k] and dist[k] == dist[cur] + 1:
                            self.fromWord[k].append(cur)
            f += 1
        if dist[wordList.index(endWord)] == 1e9: return []
        else:
            ans = self.dfs(wordList.index(endWord))
            return ans


if __name__ == "__main__":
    print Solution().findLadders(
        beginWord = "hit",
        endWord = "cog",
        wordList = ["hot","dot","dog","lot","log","cog"])
    print Solution().findLadders(
        beginWord = "hit",
        endWord = "cog",
        wordList = ["hot","dot","dog","lot","log"])
