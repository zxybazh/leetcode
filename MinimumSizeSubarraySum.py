class Solution(object):
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        ans = 1e9
        sum = 0
        last = -1
        for i, x in enumerate(nums):
            while last < i:
                if last >= 0:
                    sum -= nums[last]
                last += 1
                sum += nums[last]
            if i > 0:
                sum -= nums[i-1]
            while last < len(nums)-1 and sum < s:
                last += 1
                sum += nums[last]
            if sum < s and ans == 1e9:
                return 0
            if sum >= s:
                ans = min(ans, last - i + 1)
        return 0 if ans == 1e9 else ans

if __name__ == "__main__":
    print Solution().minSubArrayLen(s = 7, nums = [2,3,1,2,4,3])
    print Solution().minSubArrayLen(s = 100, nums = [])
