# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def process(self, root, x):
        if root is None: return None
        if root.right is not None:
            s = self.process(root.right, x)
            root.val += s
            s = root.val
            root.val += x
        else:
            s = root.val
            root.val += x
        if root.left is not None:
            s += self.process(root.left, s+x)
        return s

    def convertBST(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        self.process(root, 0)
        return root

