class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n < 2: return 1
        ti = 1
        j = 1
        for i in xrange(n-1):
            k = ti+j
            ti = j
            j = k
        return k

if __name__ == "__main__":
    print Solution().climbStairs(3)
