class Solution(object):
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        num = str(num)
        ans = ''
        for x in range(10)[::-1]:
            if str(x) in num:
                l = num.count(str(x))
                if num.startswith(str(x) * l):
                    ans += num[:l]
                    num = num[l:]
                else:
                    for i in xrange(l):
                        if num[i] != str(x):
                            last = len(num[i+1:])-num[i+1:][::-1].index(str(x))-1
                            ans += num[:i] + str(x) + num[i+1:][:last]+num[i]+num[i+1:][last+1:]
                            break
                    break
        return int(ans)


if __name__ == "__main__":
    print Solution().maximumSwap(2736)
    print Solution().maximumSwap(9973)
    print Solution().maximumSwap(99098)
    print Solution().maximumSwap(1993)
