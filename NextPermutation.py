class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        print "-" * 10
        print nums
        Flag = False
        for i in xrange(len(nums)-2, -1, -1):
            if nums[i] < nums[i+1]:
                Flag = True
                for j in xrange(len(nums)-1, i, -1):
                    if nums[i] < nums[j]:
                        temp = nums[i]
                        nums[i] = nums[j]
                        nums[j] = temp
                        for t in xrange((len(nums) - (i+1))/2):
                            temp = nums[i+1+t]
                            nums[i+1+t] = nums[len(nums)-1-t]
                            nums[len(nums) - 1 - t] = temp
                        break
                break
        if not Flag:
            for i in xrange(len(nums)/2):
                temp = nums[i]
                nums[i] = nums[len(nums)-i-1]
                nums[len(nums) - i - 1] = temp
        print nums

if __name__ == "__main__":
    Solution().nextPermutation([1, 2, 3])
    Solution().nextPermutation([3, 2, 1])
    Solution().nextPermutation([1, 1, 5])
    Solution().nextPermutation([4,2,0,2,3,2,0])
