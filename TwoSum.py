class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        nums = [(nums[i], i) for i in xrange(len(nums))]
        nums = sorted(nums)
        j = len(nums)-1
        for i in xrange(len(nums)):
            while j >= 0 and nums[i][0] + nums[j][0] > target:
                j -= 1
            if j >= 0 and nums[i][0] + nums[j][0] == target: return [nums[i][1], nums[j][1]]


if __name__ == "__main__":
    print Solution().twoSum(nums = [2, 7, 11, 15], target = 9)
    print Solution().twoSum(nums = [3, 2, 4], target = 6)

