class Solution(object):
    def floodFill(self, image, sr, sc, newColor):
        """
        :type image: List[List[int]]
        :type sr: int
        :type sc: int
        :type newColor: int
        :rtype: List[List[int]]
        """
        oldColor = image[sr][sc]
        if newColor == oldColor: return image
        q = [(sr, sc)]
        while len(q) > 0:
            x, y = q[0]
            q = q[1:]
            image[x][y] = newColor
            for tx, ty in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
                xx = x + tx
                yy = y + ty
                if 0 <= xx < len(image) and 0 <= yy < len(image[0]) and image[xx][yy] == oldColor:
                    q.append((xx, yy))
        return image

if __name__ == "__main__":
    print Solution().floodFill(image = [[1,1,1],[1,1,0],[1,0,1]], sr = 1, sc = 1, newColor = 2)
