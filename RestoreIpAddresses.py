class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        ans = []
        for i in xrange(1, len(s)):
            n0 = int(s[:i])
            if int(n0) > 255: break
            if i > 1 and s[0] == '0': continue
            for j in xrange(i+1, len(s)):
                n1 = int(s[i:j])
                if int(n1) > 255: break
                if j-i > 1 and s[i] == '0': continue
                for k in xrange(j+1, len(s)):
                    n2 = int(s[j:k])
                    if int(n2) > 255: break
                    if k-j > 1 and s[j] == '0': continue
                    n3 = int(s[k:])
                    print n0, n1, n2, n3
                    if int(n3) <= 255 and (len(s[k:]) == 1 or s[k] != '0'):
                        ans.append(".".join([str(w) for w in [n0, n1, n2, n3]]))
        return ans

if __name__ == "__main__":
    print Solution().restoreIpAddresses("25525511135")
    print Solution().restoreIpAddresses("010010")
