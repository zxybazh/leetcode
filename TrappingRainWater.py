class Solution(object):
    def lcalc(self, l, r):
        if (l >= r): return 0
        middle = self.height[l:r].index(max(self.height[l:r])) + l
        return self.lcalc(l, middle) + self.height[middle] * (r - middle) - sum(self.height[middle:r])

    def rcalc(self, l, r):
        if (l >= r): return 0
        middle = r - l - self.height[l:r][::-1].index(max(self.height[l:r])) - 1 + l
        return self.rcalc(middle+1, r) + self.height[middle] * (middle-l+1) - sum(self.height[l:middle+1])

    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        if height == []: return 0
        self.height = height
        middle = height.index(max(height))
        return self.lcalc(0, middle) + self.rcalc(middle+1, len(height))

if __name__ == "__main__":
    print Solution().trap([0,1,0,2,1,0,1,3,2,1,2,1])
    print Solution().trap([])
    print Solution().trap([1])

