class Solution(object):
    def count(self, x):
        cnt = 0
        for i in xrange(1, len(str(x))):
            cnt += i * 9 * 10 ** (i - 1)
        cnt += len(str(x))*(x-10**(len(str(x))-1)+1)
        return cnt

    def findNthDigit(self, n):
        """
        :type n: int
        :rtype: int
        """
        l = 1
        r = n
        ans = 0
        while l <= r:
            mid = (l+r)/2
            if self.count(mid) >= n:
                ans = mid
                r = mid-1
            else:
                l = mid+1
        return int(str(ans)[n-self.count(ans-1)-1])


if __name__ == "__main__":
    print Solution().findNthDigit(3)
    print Solution().findNthDigit(11)
    print Solution().findNthDigit(1000)
