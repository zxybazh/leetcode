class Solution(object):
    def findRadius(self, houses, heaters):
        """
        :type houses: List[int]
        :type heaters: List[int]
        :rtype: int
        """
        r = -1e9
        houses = sorted(houses)
        heaters = sorted(heaters)
        t = 0
        for h in houses:
            while t < len(heaters) and heaters[t] <= h:
                t += 1
            res = 1e9+7
            if t > 0:
                res = min(res, abs(h - heaters[t-1]))
            if t < len(heaters):
                res = min(res, abs(h - heaters[t]))
            r = max(r, res)
        return r


if __name__ == "__main__":
    print Solution().findRadius([1,2,3,4], [1,4])
