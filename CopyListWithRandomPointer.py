# Definition for singly-linked list with a random pointer.
class RandomListNode(object):
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None

class Solution(object):
    def copyRandomList(self, head):
        """
        :type head: RandomListNode
        :rtype: RandomListNode
        """
        if head is None: return None
        map = {}
        newHead = RandomListNode(head.label)
        newHead.random = head.random
        map[head] = newHead
        last = newHead
        currentCopying = head.next
        while currentCopying is not None:
            last.next = RandomListNode(currentCopying.label)
            last.next.random = currentCopying.random
            map[currentCopying] = last.next
            currentCopying = currentCopying.next
            last = last.next
        current = newHead
        while current is not None:
            if current.random is not None:
                current.random = map[current.random]
            current = current.next
        return newHead

