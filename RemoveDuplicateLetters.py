class Solution(object):
    def remove(self, s, c):
        ans = ''
        for x in s:
            if x != c:
                ans += x
        return ans

    def removeDuplicateLetters(self, s):
        """
        :type s: str
        :rtype: str
        """
        #print("-"*10)
        ans = ''
        cnt = len(set(s))
        while len(ans) != cnt:
            #print s
            c = min(s)
            p = s.index(c)
            flag = True
            while flag:
                flag = False
                for i, x in enumerate(s[:p]):
                    if x not in s[p:]:
                        flag = True
                        break
                if not flag:
                    s = self.remove(s[p:], c)
                    ans += c
                else:
                    c = min(s[:p])
                    p = s.index(c)
        return ans

if __name__ == "__main__":
    print Solution().removeDuplicateLetters("bcabc")
    print Solution().removeDuplicateLetters("cbacdcbc")
    print Solution().removeDuplicateLetters("cadc")
    print Solution().removeDuplicateLetters("baa")
    print Solution().removeDuplicateLetters("bbcaac")
    print Solution().removeDuplicateLetters("acacb")
    print Solution().removeDuplicateLetters("bcbac")
    print Solution().removeDuplicateLetters("bcabc")
    print Solution().removeDuplicateLetters("abacb")
    print Solution().removeDuplicateLetters("thesqtitxyetpxloeevdeqifkz")
