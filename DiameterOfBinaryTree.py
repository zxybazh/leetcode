# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    def dfs(self, x):
        if x.left is not None:
            resl = self.dfs(x.left)
        else:
            resl = (-1, 0)
        if x.right is not None:
            resr = self.dfs(x.right)
        else:
            resr = (-1, 0)
        r0 = max(resl[0], resr[0]) + 1
        r1 = max(resl[1], resr[1], resl[0]+resr[0]+2)
        return r0, r1

    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None: return 0
        return self.dfs(root)[1]
