class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        ans = []
        res = []
        for i, x in enumerate(strs):
            res.append((i, sorted(x)))
        res = sorted(res, key=lambda x:x[1])
        last = None
        for i, x in enumerate(res):
            if x[1] != last:
                ans.append([strs[x[0]]])
            else:
                ans[-1].append(strs[x[0]])
            last = x[1]
        return ans

if __name__ == "__main__":
    print Solution().groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"])
