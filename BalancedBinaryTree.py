# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def check(self, root):
        if root is None:
            return 0, True
        else:
            ll = self.check(root.left)
            rr = self.check(root.right)
            return max(ll[0], rr[0])+1, ll[1] and rr[1] and abs(ll[0]-rr[0]) <= 1

    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root is None:
            return True
        else:
            return self.check(root)[1]

